package com.lianda.demo.pattern.strategy;

public class CalTest {
    public static void main(String[] args) {
        int add = Calculator.ADD.exec(1, 2);
        System.out.println("add result====>" + add);

        int sub = Calculator.SUB.exec(10, 5);
        System.out.println("sub result====>" + sub);
    }
}
