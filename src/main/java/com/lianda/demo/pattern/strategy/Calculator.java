package com.lianda.demo.pattern.strategy;

//另一种策略枚举
public enum Calculator {

    ADD("add"){
        @Override
        public int exec(int a, int b) {
            return a + b;
        }
    },
    SUB("subtract"){
        @Override
        public int exec(int a, int b) {
            return a - b;
        }
    };

    public abstract int exec(int a, int b);

    //运算符
    private String value = "";

    private Calculator(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
