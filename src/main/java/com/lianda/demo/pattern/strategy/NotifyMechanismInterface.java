package com.lianda.demo.pattern.strategy;

//通知机制的接口
public interface NotifyMechanismInterface {

    public void doNotify(String msg);

    public static NotifyMechanismInterface byEmail() {
       return new NotifyMechanismInterface() {
           @Override
           public void doNotify(String msg) {
               System.out.println("内容：" + msg +"，邮件通知已经发送...");
           }
       };
    }

    public static NotifyMechanismInterface bySms() {
        return new NotifyMechanismInterface() {
            @Override
            public void doNotify(String msg) {
                System.out.println("内容：" + msg +"，短信通知已经发送...");
            }
        };
    }

    public static NotifyMechanismInterface byWechat() {
        return new NotifyMechanismInterface() {
            @Override
            public void doNotify(String msg) {
                System.out.println("内容：" + msg +"，微信通知已经发送...");
            }
        };
    }
}
