package com.lianda.demo.pattern.strategy;

import java.util.Arrays;

//rich enum type 策略模式
//不复杂的场景可以使用
public enum NotifyType {
    email("邮件", NotifyMechanismInterface.byEmail()),
    sms("短信", NotifyMechanismInterface.bySms()),
    wechat("微信", NotifyMechanismInterface.byWechat());

    String memo;
    NotifyMechanismInterface notifyMechanism;

    private NotifyType(String memo, NotifyMechanismInterface notifyMechanism) {
        this.memo = memo;
        this.notifyMechanism = notifyMechanism;
    }

    public String getMemo() {
        return memo;
    }

    public NotifyMechanismInterface getNotifyMechanism() {
        return notifyMechanism;
    }

    //获取具体类型
    public static NotifyType getType(String type) {
        NotifyType resultType = null;
        NotifyType[] typeArray = NotifyType.values();
        for (NotifyType notifyType : typeArray) {
            if (notifyType.name().equals(type)) {
                resultType = notifyType;
                break;
            }
        }
        //找不到的情况下，默认发短信
        if (null == resultType) {
            System.out.println("找不到具体类型，默认发短信");
            resultType = NotifyType.sms;
        }

        return resultType;
    }

}
