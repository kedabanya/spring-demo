package com.lianda.demo.pattern.strategy;

public class StrategyTest {
    public static void main(String[] args) {
        //Enum.valueOf根据枚举变量名的字符串值获取枚举变量
        //使用策略模式
        String type = "email";
        String msg = "Success Login!";
        NotifyType.getType(type)
                .getNotifyMechanism()
                .doNotify(msg);

        String type2 = "sms";
        NotifyType.getType(type2)
                .getNotifyMechanism()
                .doNotify(msg);

        String type3 = "wechat";
        NotifyType.getType(type3)
                .getNotifyMechanism()
                .doNotify(msg);

        String type4 = "unknown";
        NotifyType.getType(type4)
                .getNotifyMechanism()
                .doNotify(msg);
    }
}
