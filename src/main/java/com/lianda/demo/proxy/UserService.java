package com.lianda.demo.proxy;

import com.lianda.demo.proxy.annotation.DataSourceSelector;
import com.lianda.demo.proxy.common.DynamicDataSourceEnum;
import com.lianda.demo.proxy.domain.User;
import com.lianda.demo.proxy.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    @Autowired
    private UserMapper userMapper;

    @DataSourceSelector(value = DynamicDataSourceEnum.SLAVE)
    public List<User> listUser() {
        List<User> users = userMapper.selectAll();
        return users;
    }

    @DataSourceSelector(value = DynamicDataSourceEnum.MASTER)
    public int update() {
        User user = new User();
        user.setUserId(Long.parseLong("1196978513958141952"));
        user.setUserName("修改后的名字1");
        return userMapper.updateByPrimaryKeySelective(user);
    }

}
