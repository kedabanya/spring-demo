package com.lianda.demo.proxy.common;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

//AbstractRoutingDataSource实现基于key路由到对应数据源
public class DynamicDataSource extends AbstractRoutingDataSource {

    @Override
    protected Object determineCurrentLookupKey() {
        return DataSourceContextHolder.get();
    }
}
