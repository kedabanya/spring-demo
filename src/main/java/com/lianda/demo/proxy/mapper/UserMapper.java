package com.lianda.demo.proxy.mapper;

import com.lianda.demo.proxy.common.MyMapper;
import com.lianda.demo.proxy.domain.User;

public interface UserMapper extends MyMapper<User> {}
