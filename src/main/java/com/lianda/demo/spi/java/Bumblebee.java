package com.lianda.demo.spi.java;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Bumblebee implements Robot {

    @Override
    public void sayHello() {
        System.out.println("Hello, I am Bumblebee.");
    }
}
