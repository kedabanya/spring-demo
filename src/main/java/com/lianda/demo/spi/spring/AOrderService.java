package com.lianda.demo.spi.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AOrderService implements OrderService {

    @Autowired
    private WareService wareService;

    @Override
    public void getOrder(String msg) {
        wareService.getWare("a order -> " + msg);
    }
}
