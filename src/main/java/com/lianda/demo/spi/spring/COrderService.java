package com.lianda.demo.spi.spring;

import org.springframework.stereotype.Service;

@Service
public class COrderService implements OrderService {

    @Override
    public void getOrder(String msg) {
        System.out.println("c order -> " + msg);
    }
}
