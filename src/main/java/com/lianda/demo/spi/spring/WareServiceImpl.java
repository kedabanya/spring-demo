package com.lianda.demo.spi.spring;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class WareServiceImpl implements WareService {

    @Override
    public void getWare(String msg) {
        System.out.println("getWare print: " + msg);
    }
}
