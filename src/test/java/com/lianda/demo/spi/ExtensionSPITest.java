package com.lianda.demo.spi;

import com.lianda.demo.spi.spring.OrderService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 自行实现的Spring SPI
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ExtensionSPITest {

    @Autowired
    private ExtensionLoader extLoader;

    @Test
    public void sayHello() throws Exception {
        OrderService japan = (OrderService) extLoader.getExtensionLoader(OrderService.class).get("japan");
        japan.getOrder("hello.");

        OrderService china = (OrderService) extLoader.getExtensionLoader(OrderService.class).get("china");
        china.getOrder("worder.");
    }
}
