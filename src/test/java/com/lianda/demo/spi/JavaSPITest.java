package com.lianda.demo.spi;

import com.lianda.demo.spi.java.Robot;
import org.junit.Test;

import java.util.ServiceLoader;

/**
 * Java SPI 机制，使用ServiceLoader，读取META-INF/services下文件
 */
public class JavaSPITest {

    @Test
    public void sayHello() throws Exception {
        ServiceLoader<Robot> serviceLoader = ServiceLoader.load(Robot.class);
        for (Robot robot : serviceLoader) {
            robot.sayHello();
        }
    }
}
