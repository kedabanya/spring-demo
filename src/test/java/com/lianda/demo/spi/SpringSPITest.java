package com.lianda.demo.spi;

import com.lianda.demo.spi.spring.AOrderService;
import com.lianda.demo.spi.spring.BOrderService;
import com.lianda.demo.spi.spring.OrderService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.support.SpringFactoriesLoader;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * Springboot，类SPI，读取META-INF/spring.factories；
 * 对于bean内@autowire注解，无法完成自动装配；因为是通过直接反射生成bean的，bean没有被spring容器管理；
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringSPITest {

    @Autowired
    private AOrderService aOrderService;

    @Autowired
    private BOrderService bOrderService;

    @Test
    public void sayHello() throws Exception {
        List<OrderService> services = SpringFactoriesLoader.loadFactories(OrderService.class, null);
        for (OrderService service : services) {
            service.getOrder("worder.");
        }
    }
}
